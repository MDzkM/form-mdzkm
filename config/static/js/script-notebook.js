var split_json;
var price_total;

function load_json() {
  $.ajax({
    url:"/notebook/notebook.json",
    success:function(json_file) {
      split_json = json_file.local_json.slice(0, 10)
      $.each(split_json, function(index, value) {
        $("#tbody").append('<tr><th scope="row">' + (index + 1) + '</th><td>' + value.id + '</th><td>' + value.name + '</td><td>' + value.details + '</td><td>' + value.price + '</td><td><button class="btn btn-primary" id="' + value.price + '" onclick="buttonclick(this)">Add to wishlist</button></td></tr>')
      })
    }
  })
}

function buttonclick(object) {
  var item_price = $(object).attr("id")
  var current_price = $("#total").text().split(": ").slice(-1)
  if ($(object).text() === "Add to wishlist") {
    price_total = parseInt(item_price) + parseInt(current_price)
    $("#total").text("Jumlah uang yang saya harus siapkan: " + price_total)
    $(object).text("Remove from wishlist")
  }
  else {
    price_total = parseInt(current_price) - parseInt(item_price)
    $("#total").text("Jumlah uang yang saya harus siapkan: " + price_total)
    $(object).text("Add to wishlist")
  }
}

$("body").addClass(localStorage.getItem("theme"));
$("table").addClass(localStorage.getItem("table-theme"));

setTimeout(function() {
  $("#loader").css("display", "none");
  $("#loader-background").css("display", "none");
  $("#container").css("display", "block");
}, 3000);

if ($('body').attr('class') === 'light-toggled') {
  $('#dark_theme').hover(
    function() {
      $('body').addClass('dark')
      $('table').addClass('table-dark')
    },
    function() {
      $('body').removeClass('dark')
      $('table').removeClass('table-dark')
    }
  );
}

if ($('body').attr('class') === 'dark-toggled') {
  $('#light_theme').hover(
    function() {
      $('body').addClass('light')
      $('table').removeClass('table-dark')
    },
    function() {
      $('body').removeClass('light')
      $('table').addClass('table-dark')
    }
  );
}

$(document).ready(function() {
    $("#dark_theme").click(function() {
        localStorage.setItem("theme", "dark-toggled");
        localStorage.setItem("table-theme", "table-dark");
        $("table").toggleClass("table-dark");
        $("body").removeClass("light-toggled");
        $("body").toggleClass("dark-toggled");
    });
});

$(document).ready(function() {
    $("#light_theme").click(function() {
        localStorage.setItem("theme", "light-toggled");
        localStorage.setItem("table-theme", "");
        $("table").removeClass("table-dark");
        $("body").removeClass("dark-toggled");
        $("body").toggleClass("light-toggled");
    });
});
