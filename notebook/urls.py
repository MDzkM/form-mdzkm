from django.urls import path
from .views import *

urlpatterns = [
    path('', notebook_list, name='notebook_list'),
    path('notebook.json', get_json, name='notebook.json'),
    path('wishlist', wishlist, name='wishlist'),
]
