from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def notebook_list(request):
    if 'item' not in request.session:
        request.session['item'] = ''
    response = render(request, 'notebook_list.html')
    response.set_cookie('id_laptop', request.session['item'])
    return response

def wishlist(request):
    if request.method == 'POST':
        data = request.POST.get('data')
        id_laptop = request.session['item'].split()
        if data in id_laptop:
            id_laptop.pop(id_laptop.index(data))
        else:
            id_laptop.append(data)
        request.session['item'] = ' '.join(id_laptop)

    return JsonResponse({'data':'OK'})

def get_json(request):
    url = "https://enterkomputer.com/api/product/notebook.json"
    notebook_json = requests.get(url).json()
    notebook_arr = []
    for notebook in notebook_json:
        if "Win 10" in notebook["details"]:
            notebook_arr.append(
                {
                    "id" : notebook["id"],
                    "name" : notebook["name"],
                    "details" : notebook["details"],
                    "price" : notebook["price"]
                }
            )
    return JsonResponse({"local_json" : notebook_arr})
