from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from django.urls import resolve
from django.http import HttpRequest
from . import views
import unittest
import time
import re

class FunctionalTest2(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest2, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_check_title(self):
        self.browser.get(self.live_server_url + "/notebook")
        time.sleep(5)
        self.assertIn("Notebook List", self.browser.title)
        self.tearDown()

    def test_wishlist(self):
        self.browser.get(self.live_server_url + "/notebook")
        time.sleep(120)
        total_price = self.browser.find_element_by_id("total").text.split(": ")[-1]
        self.assertEqual(int(total_price), 0)
        element = self.browser.find_element_by_xpath('//*[@id="3350000"]')
        time.sleep(5)
        element.send_keys(Keys.RETURN)
        time.sleep(5)
        total_price = self.browser.find_element_by_id("total").text.split(": ")[-1]
        self.assertEqual(int(total_price), 3350000)
        element = self.browser.find_element_by_xpath('//*[@id="3350000"]')
        time.sleep(5)
        element.send_keys(Keys.RETURN)
        time.sleep(5)
        total_price = self.browser.find_element_by_id("total").text.split(": ")[-1]
        self.assertEqual(int(total_price), 0)

class FormTest2(TestCase):

    def test_notebook_page_response(self):
        self.response = Client().get('/notebook')
        self.assertEqual(self.response.status_code, 200)

    def test_url_name(self):
        self.found = resolve('/notebook')
        self.assertEqual(self.found.url_name, 'notebook_list')

    def test_notebook_page_response(self):
        self.response = Client().get('/notebook/notebook.json')
        self.assertEqual(self.response.status_code, 200)

    def test_url_name(self):
        self.found = resolve('/notebook/notebook.json')
        self.assertEqual(self.found.url_name, 'notebook.json')
