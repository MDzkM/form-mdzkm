from django import forms
from .models import Status

class StatusForm(forms.Form):
    myStatus = forms.CharField(label="My status", max_length=300, required=True)
