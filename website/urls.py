from django.urls import path
from .views import *

APP_NAME = 'home'

urlpatterns = [
    path('', index, name='index'),
    path('profile/', profile, name='profile'),
    path('logout/', auth_logout, name='logout'),
]
