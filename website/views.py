from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import redirect, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from django.contrib.auth import logout
from django.urls import reverse

from .models import *
from .forms import *

def index(request):
    if request.method == 'POST':
        response = {}
        form = StatusForm(request.POST)

        if form.is_valid():
            response['myStatus'] = form.cleaned_data['myStatus']
            myStatus = Status(
                    myStatus=response['myStatus'],
                )
            myStatus.save()

    else:
        form = StatusForm()

    status_form_context = {
        'all_status': Status.objects.all().values(),
        'status_form': form}
    return render(request, 'index.html', status_form_context)

def profile(request):
    return render(request, 'profile.html')

@login_required
def auth_logout(request):
    logout(request)
    response = HttpResponseRedirect(reverse('index'))
    return response
