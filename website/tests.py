from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from django.urls import resolve
from django.http import HttpRequest
from .models import *
from .forms import *
from . import views
import unittest
import time
import re

# Create your tests here.

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_check_title(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        self.assertIn("Dzikra's Form", self.browser.title)
        self.tearDown()

    def test_css_background(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        element = self.browser.find_element_by_css_selector('body')
        cssprop = element.value_of_css_property('background-color')
        self.assertEqual(cssprop, "rgba(255, 255, 255, 1)")
        self.tearDown()

    def test_add_status(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        status = self.browser.find_element_by_id('id_myStatus')
        submit = self.browser.find_element_by_id('submit_btn')
        status.send_keys('Hello World!')
        time.sleep(3)
        submit.send_keys(Keys.RETURN)
        self.browser.get(self.live_server_url)
        self.assertIn('Hello World!', self.browser.page_source)
        self.tearDown()

    def test_profile_button_exist(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        profile_button = self.browser.find_element_by_id('profile_btn')
        self.assertIn(profile_button.text, "Go To Profile")
        self.tearDown()

    def test_navigate_to_profile_page(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        profile_page_button = self.browser.find_element_by_link_text('Go To Profile')
        profile_page_button.click()
        time.sleep(3)
        self.assertIn('Dzikra\'s Profile', self.browser.title)

    def test_profile_page_content(self):
        self.browser.get(self.live_server_url + "/profile")
        time.sleep(5)
        body = self.browser.find_element_by_tag_name('body')
        bgbody = body.value_of_css_property('background-color')
        time.sleep(3)
        self.assertEqual(bgbody, 'rgba(255, 255, 255, 1)')
        self.assertIn('Muhammad Dzikra Muzaki', self.browser.page_source)

    # These are the tests for in-class challenge
    def test_landing_page_button_color(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        profile_btn = self.browser.find_element_by_tag_name('button')
        cssprop = profile_btn.value_of_css_property('background-color')
        self.assertEqual(cssprop, 'rgba(0, 123, 255, 1)')
        self.tearDown()

    def test_back_button_exist(self):
        self.browser.get(self.live_server_url + "/profile")
        time.sleep(5)
        back_button = self.browser.find_element_by_id('back_btn')
        self.assertIn(back_button.text, "Back")
        self.tearDown()

    def test_profile_page_button_color(self):
        self.browser.get(self.live_server_url + "/profile")
        time.sleep(5)
        back_button = self.browser.find_element_by_id('back_btn')
        cssprop = back_button.value_of_css_property('background-color')
        self.assertEqual(cssprop, 'rgba(0, 123, 255, 1)')
        self.tearDown()

    def test_landing_page_welcome_message(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        self.assertIn('Apa kabar?', self.browser.page_source)
        self.tearDown()

    def test_form_is_above_submit_button(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        form_is_above_regex = re.compile("<form [\s\S]* value=\"submit\" id=\"submit_btn\" />")
        self.assertTrue(form_is_above_regex.search(self.browser.page_source))
        self.tearDown()

    def test_status_is_below_submit_button(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        status = self.browser.find_element_by_id('id_myStatus')
        submit = self.browser.find_element_by_id('submit_btn')
        status.send_keys('Hi!')
        time.sleep(3)
        submit.send_keys(Keys.RETURN)
        status_is_above_regex = re.compile("value=\"submit\"[\s\S]* <div class=\"col-sm-4 form-content\">Hi!</div>")
        self.assertTrue(status_is_above_regex.search(self.browser.page_source))
        self.tearDown()

    #   Newly added tests for Story 8 requirements
    def test_dark_theme(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        dark_theme_button = self.browser.find_element_by_id('dark_theme')
        dark_theme_button.send_keys(Keys.RETURN)
        time.sleep(10)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(52, 58, 64, 1)')
        self.tearDown()

    def test_dark_theme_hover(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        action = ActionChains(self.browser)
        dark_theme_button = self.browser.find_element_by_id('dark_theme')
        action.move_to_element(dark_theme_button).perform()
        time.sleep(10)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(52, 58, 64, 1)')
        self.tearDown()

    def test_light_theme(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        light_theme_button = self.browser.find_element_by_id('light_theme')
        light_theme_button.send_keys(Keys.RETURN)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')
        self.tearDown()

    def test_light_theme_hover(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        action = ActionChains(self.browser)
        light_theme_button = self.browser.find_element_by_id('light_theme')
        action.move_to_element(light_theme_button).perform()
        time.sleep(5)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')
        self.tearDown()

    # def test_theme_stored_in_local_storage(self):
    #     self.browser.get(self.live_server_url)
    #     time.sleep(5)
    #     dark_theme_button = self.browser.find_element_by_id('dark_theme')
    #     dark_theme_button.send_keys(Keys.RETURN)
    #
    #     self.browser.get(self.live_server_url + '/profile/')
    #     time.sleep(5)
    #     body = self.browser.find_element_by_tag_name('body')
    #     body_class = body.get_attribute('class')
    #
    #     self.assertEqual(body_class, 'dark-toggled')
    #
    #     light_theme_button = self.browser.find_element_by_id('light_theme')
    #     dropdown.send_keys(Keys.RETURN)
    #
    #     self.browser.get(self.live_server_url)
    #     time.sleep(5)
    #     body = self.browser.find_element_by_tag_name('body')
    #     body_class= body.get_attribute('class')
    #
    #     self.assertEqual(body_class, 'light-toggled')
    #     self.tearDown()

class FormTest(TestCase):

    def test_landing_page_response(self):
        self.response = Client().get('/')
        self.assertEqual(self.response.status_code, 200)

    def test_url_name(self):
        self.found = resolve('/')
        self.assertEqual(self.found.url_name, 'index')

    def test_intro_is_exist(self):
        self.response = Client().get('/')
        self.assertIn('<h1>Apa kabar?</h1>', self.response.content.decode())

    def test_form_is_exist(self):
        self.response = Client().get('/')
        self.assertIn('</form>', self.response.content.decode())

    def test_template_is_exist(self):
        self.response = Client().get('/error')
        self.assertEqual(self.response.status_code, 404)

    def test_button_is_exist(self):
        self.response = Client().get('/')
        self.assertIn('</button>', self.response.content.decode())

    def test_form_function(self):
        self.found = resolve('/')
        self.assertEqual(self.found.func, views.index)

    def test_post_is_valid(self):
        response = Client().post('/', {'myStatus': 'Quite alright.'})
        self.assertIn('Quite alright.', response.content.decode())

    def test_response_is_posted(self):
        self.response = Client().post('/', {'myStatus' : 'Fine, thank you.'})
        self.assertEqual(self.response.status_code, 200)

    def test_create_models(self):
        before = Status.objects.all().count()
        Status.objects.create(myStatus='Confused.')
        after = Status.objects.all().count()
        self.assertNotEqual(before, after)

    def test_profile_page_response(self):
        self.response = Client().get('/profile')
        self.assertEqual(self.response.status_code, 200)

    def test_profile_url_name(self):
        self.found = resolve('/profile')
        self.assertEqual(self.found.url_name, 'profile')

    def test_header_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<h1>My Profile</h1>', self.response.content.decode())

    def test_name_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<p>Nama: Muhammad Dzikra Muzaki</p>', self.response.content.decode())

    def test_npm_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<p>NPM: 1806141334</p>', self.response.content.decode())

    def test_photo_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<img src="../static/images/My_Photo.png" class="img-opt" alt="MyPhoto">', self.response.content.decode())

    def test_profile_button_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('</button>', self.response.content.decode())

    #   Newly added tests for Story 8 requirements
    def test_light_theme_button_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<button type="button" class="btn btn-primary" id="light_theme">Light</button>', self.response.content.decode())

    def test_dark_theme_button_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<button type="button" class="btn btn-primary" id="dark_theme">Dark</button>', self.response.content.decode())

    def test_accordion_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<button class="accordion">Activity / Experience</button>', self.response.content.decode())

    def test_accordion_panel_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<div class="panel">', self.response.content.decode())

    def test_accordion_content_is_exist(self):
        self.response = Client().get('/profile')
        self.assertIn('<p class="accordion-content">', self.response.content.decode())
